﻿using Moq;
using NUnit.Framework;
using RobotController.Models;
using RobotController.Models.Enums;
using RobotController.Services;
using RobotController.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;

namespace RobotController.Test.ViewModels
{
    [TestFixture]
    class MainWindowViewModelTest
    {
        private MainWindowViewModel mainWindowViewModel;

        private Mock<IBluetoothClientService> bluetoothClientServiceMock;
        private Mock<IBluetoothConnectionSupervisionService> bluetoothConnectionSupervisionServiceMock;
        private Mock<IBluetoothModuleModel> bluetoothModuleModelMock;
        private Mock<ILightDiodeModel> frontLightDiodeModelMock;
        private Mock<IMotorModel> leftMotorModelMock;
        private Mock<IMotorModel> rightMotorModelMock;
        private IBluetoothClientService bluetoothClientService;
        private IBluetoothConnectionSupervisionService bluetoothConnectionSupervisionService;
        private IBluetoothModuleModel bluetoothModuleModel;
        private ILightDiodeModel frontLightDiodeModel;
        private IMotorModel leftMotorModel;
        private IMotorModel rightMotorModel;

        [SetUp]
        public void Init()
        {
            bluetoothClientServiceMock = new Mock<IBluetoothClientService>();
            bluetoothClientServiceMock.Setup(o => o.Connected).Returns(true);
            bluetoothClientServiceMock.Setup(o => o.SendControlCommandAsync(It.IsAny<ControlCommand>())).ReturnsAsync(true);
            bluetoothClientService = bluetoothClientServiceMock.Object;

            bluetoothConnectionSupervisionServiceMock = new Mock<IBluetoothConnectionSupervisionService>();
            bluetoothConnectionSupervisionService = bluetoothConnectionSupervisionServiceMock.Object;

            bluetoothModuleModelMock = new Mock<IBluetoothModuleModel>();
            bluetoothModuleModelMock.SetupAllProperties();
            bluetoothModuleModel = bluetoothModuleModelMock.Object;

            frontLightDiodeModelMock = new Mock<ILightDiodeModel>();
            frontLightDiodeModelMock.SetupAllProperties();
            frontLightDiodeModel = frontLightDiodeModelMock.Object;

            leftMotorModelMock = new Mock<IMotorModel>();
            leftMotorModelMock.SetupAllProperties();
            leftMotorModel = leftMotorModelMock.Object;

            rightMotorModelMock = new Mock<IMotorModel>();
            rightMotorModelMock.SetupAllProperties();
            rightMotorModel = rightMotorModelMock.Object;

            mainWindowViewModel = new MainWindowViewModel(bluetoothClientService, bluetoothConnectionSupervisionService, bluetoothModuleModel, frontLightDiodeModel, leftMotorModel, rightMotorModel);
        }

        [Test]
        public void WindowLoadedCommandTest()
        {
            bool result = false;
            CommandManager.RequerySuggested += (s, e) => result = true;

            mainWindowViewModel.WindowLoadedCommand.Execute(null);
            ProcessDispatcherWorkItemsInBackground();

            bluetoothClientServiceMock.Verify(o => o.ConnectAsync(), Times.Once);
            bluetoothClientServiceMock.Verify(o => o.Connected, Times.Once);
            bluetoothConnectionSupervisionServiceMock.Verify(o => o.Start(), Times.Once);

            Assert.AreEqual(ConnectionState.Connected, mainWindowViewModel.BluetoothModuleModel.Connected);
            Assert.IsTrue(result);
        }

        [Test]
        public void WindowClosingCommandTest()
        {
            mainWindowViewModel.WindowClosingCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.CloseAsync(), Times.Once);
            bluetoothConnectionSupervisionServiceMock.Verify(o => o.Stop(), Times.Once);
        }

        [TestCase(Key.L)]
        [TestCase(Key.Left)]
        [TestCase(Key.Right)]
        [TestCase(Key.Up)]
        [TestCase(Key.Down)]
        public void WindowKeyDownCommandTest(Key key)
        {
            mainWindowViewModel.WindowLoadedCommand.Execute(null);

            bool canExecute = mainWindowViewModel.WindowKeyDownCommand.CanExecute(null);
            Assert.IsTrue(canExecute);

            Assert.IsFalse(mainWindowViewModel.LeftKeyChecked);
            Assert.IsFalse(mainWindowViewModel.RightKeyChecked);
            Assert.IsFalse(mainWindowViewModel.UpKeyChecked);
            Assert.IsFalse(mainWindowViewModel.DownKeyChecked);

            mainWindowViewModel.WindowKeyDownCommand.Execute(new KeyEventArgs(null, new Mock<PresentationSource>().Object, 0, key));

            switch (key)
            {
                case Key.L:
                    bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.FrontDiodeOn), Times.Once());
                    Assert.AreEqual(mainWindowViewModel.FrontDiodeModel.Activated, mainWindowViewModel.LKeyChecked);
                    Assert.IsTrue(mainWindowViewModel.FrontDiodeModel.Activated);
                    break;
                case Key.Left:
                    bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.DriveLeft), Times.Once());
                    Assert.IsTrue(mainWindowViewModel.LeftKeyChecked);
                    Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.Backward);
                    Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.Forward);
                    break;
                case Key.Right:
                    bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.DriveRight), Times.Once());
                    Assert.IsTrue(mainWindowViewModel.RightKeyChecked);
                    Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.Forward);
                    Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.Backward);
                    break;
                case Key.Up:
                    bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.DriveForward), Times.Once());
                    Assert.IsTrue(mainWindowViewModel.UpKeyChecked);
                    Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.Forward);
                    Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.Forward);
                    break;
                case Key.Down:
                    bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.DriveBackward), Times.Once());
                    Assert.IsTrue(mainWindowViewModel.DownKeyChecked);
                    Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.Backward);
                    Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.Backward);
                    break;
            }
        }
        
        [TestCase(Key.Left)]
        [TestCase(Key.Right)]
        [TestCase(Key.Up)]
        [TestCase(Key.Down)]
        public void WindowKeyUpCommandTest(Key key)
        {
            mainWindowViewModel.WindowLoadedCommand.Execute(null);

            bool canExecute = mainWindowViewModel.WindowKeyUpCommand.CanExecute(null);
            Assert.IsTrue(canExecute);

            mainWindowViewModel.WindowKeyDownCommand.Execute(new KeyEventArgs(null, new Mock<PresentationSource>().Object, 0, key));
            mainWindowViewModel.WindowKeyUpCommand.Execute(new KeyEventArgs(null, new Mock<PresentationSource>().Object, 0, key));

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.StopMotors), Times.Once());
            Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.None);
            Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.None);

            switch (key)
            {
                case Key.Left:
                    Assert.IsFalse(mainWindowViewModel.LeftKeyChecked);
                    break;
                case Key.Right:
                    Assert.IsFalse(mainWindowViewModel.RightKeyChecked);
                    break;
                case Key.Up:
                    Assert.IsFalse(mainWindowViewModel.UpKeyChecked);
                    break;
                case Key.Down:
                    Assert.IsFalse(mainWindowViewModel.DownKeyChecked);
                    break;
            }
        }

        [Test]
        public void LKeyButtonPressCommandTest()
        {
            mainWindowViewModel.WindowLoadedCommand.Execute(null);

            bool canExecute = mainWindowViewModel.LKeyButtonPressCommand.CanExecute(null);
            Assert.IsTrue(canExecute);

            mainWindowViewModel.LKeyChecked = true;
            mainWindowViewModel.LKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.FrontDiodeOn), Times.Once());
            Assert.AreEqual(mainWindowViewModel.FrontDiodeModel.Activated, mainWindowViewModel.LKeyChecked);
            Assert.IsTrue(mainWindowViewModel.FrontDiodeModel.Activated);

            mainWindowViewModel.LKeyChecked = false;
            mainWindowViewModel.LKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.FrontDiodeOff), Times.Once());
            Assert.AreEqual(mainWindowViewModel.FrontDiodeModel.Activated, mainWindowViewModel.LKeyChecked);
            Assert.IsFalse(mainWindowViewModel.FrontDiodeModel.Activated);
        }

        [Test]
        public void LeftKeyButtonPressCommandTest()
        {
            mainWindowViewModel.WindowLoadedCommand.Execute(null);

            bool canExecute = mainWindowViewModel.LeftKeyButtonPressCommand.CanExecute(null);
            Assert.IsTrue(canExecute);

            mainWindowViewModel.LeftKeyChecked = true;
            mainWindowViewModel.LeftKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.DriveLeft), Times.Once());
            Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.Backward);
            Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.Forward);
            Assert.IsFalse(mainWindowViewModel.RightKeyChecked);
            Assert.IsFalse(mainWindowViewModel.UpKeyChecked);
            Assert.IsFalse(mainWindowViewModel.DownKeyChecked);

            mainWindowViewModel.LeftKeyChecked = false;
            mainWindowViewModel.LeftKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.StopMotors), Times.Once());
            Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.None);
            Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.None);
        }

        [Test]
        public void RightKeyButtonPressCommandTest()
        {
            mainWindowViewModel.WindowLoadedCommand.Execute(null);

            bool canExecute = mainWindowViewModel.RightKeyButtonPressCommand.CanExecute(null);
            Assert.IsTrue(canExecute);

            mainWindowViewModel.RightKeyChecked = true;
            mainWindowViewModel.RightKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.DriveRight), Times.Once());
            Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.Forward);
            Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.Backward);
            Assert.IsFalse(mainWindowViewModel.LeftKeyChecked);
            Assert.IsFalse(mainWindowViewModel.UpKeyChecked);
            Assert.IsFalse(mainWindowViewModel.DownKeyChecked);

            mainWindowViewModel.RightKeyChecked = false;
            mainWindowViewModel.RightKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.StopMotors), Times.Once());
            Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.None);
            Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.None);
        }

        [Test]
        public void UpKeyButtonPressCommandTest()
        {
            mainWindowViewModel.WindowLoadedCommand.Execute(null);

            bool canExecute = mainWindowViewModel.UpKeyButtonPressCommand.CanExecute(null);
            Assert.IsTrue(canExecute);

            mainWindowViewModel.UpKeyChecked = true;
            mainWindowViewModel.UpKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.DriveForward), Times.Once());
            Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.Forward);
            Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.Forward);
            Assert.IsFalse(mainWindowViewModel.LeftKeyChecked);
            Assert.IsFalse(mainWindowViewModel.RightKeyChecked);
            Assert.IsFalse(mainWindowViewModel.DownKeyChecked);

            mainWindowViewModel.UpKeyChecked = false;
            mainWindowViewModel.UpKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.StopMotors), Times.Once());
            Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.None);
            Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.None);
        }

        [Test]
        public void DownKeyButtonPressCommandTest()
        {
            mainWindowViewModel.WindowLoadedCommand.Execute(null);

            bool canExecute = mainWindowViewModel.DownKeyButtonPressCommand.CanExecute(null);
            Assert.IsTrue(canExecute);

            mainWindowViewModel.DownKeyChecked = true;
            mainWindowViewModel.DownKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.DriveBackward), Times.Once());
            Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.Backward);
            Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.Backward);
            Assert.IsFalse(mainWindowViewModel.LeftKeyChecked);
            Assert.IsFalse(mainWindowViewModel.RightKeyChecked);
            Assert.IsFalse(mainWindowViewModel.UpKeyChecked);

            mainWindowViewModel.DownKeyChecked = false;
            mainWindowViewModel.DownKeyButtonPressCommand.Execute(null);

            bluetoothClientServiceMock.Verify(o => o.SendControlCommandAsync(ControlCommand.StopMotors), Times.Once());
            Assert.AreEqual(mainWindowViewModel.LeftMotorModel.MotorDirection, MotorDirection.None);
            Assert.AreEqual(mainWindowViewModel.RightMotorModel.MotorDirection, MotorDirection.None);
        }

        [Test]
        public void LKeyCheckedTest()
        {
            bool result = false;
            mainWindowViewModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "LKeyChecked")
                {
                    result = true;
                }
            };

            mainWindowViewModel.LKeyChecked = false;
            mainWindowViewModel.LKeyChecked = true;
            Assert.That(result, Is.EqualTo(true));
        }
        
        [Test]
        public void LeftKeyCheckedTest()
        {
            bool result = false;
            mainWindowViewModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "LeftKeyChecked")
                {
                    result = true;
                }
            };

            mainWindowViewModel.LeftKeyChecked = false;
            mainWindowViewModel.LeftKeyChecked = true;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void RightKeyCheckedTest()
        {
            bool result = false;
            mainWindowViewModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "RightKeyChecked")
                {
                    result = true;
                }
            };

            mainWindowViewModel.RightKeyChecked = false;
            mainWindowViewModel.RightKeyChecked = true;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void UpKeyCheckedTest()
        {
            bool result = false;
            mainWindowViewModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "UpKeyChecked")
                {
                    result = true;
                }
            };

            mainWindowViewModel.UpKeyChecked = false;
            mainWindowViewModel.UpKeyChecked = true;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void DownKeyCheckedTest()
        {
            bool result = false;
            mainWindowViewModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "DownKeyChecked")
                {
                    result = true;
                }
            };

            mainWindowViewModel.DownKeyChecked = false;
            mainWindowViewModel.DownKeyChecked = true;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void BluetoothModuleModelGetTest()
        {
            Assert.AreSame(bluetoothModuleModel, mainWindowViewModel.BluetoothModuleModel);
        }

        [Test]
        public void FrontDiodeModelGetTest()
        {
            Assert.AreSame(frontLightDiodeModel, mainWindowViewModel.FrontDiodeModel);
        }

        [Test]
        public void LeftMotorModelGetTest()
        {
            Assert.AreSame(leftMotorModel, mainWindowViewModel.LeftMotorModel);
        }

        [Test]
        public void RightMotorModelGetTest()
        {
            Assert.AreSame(rightMotorModel, mainWindowViewModel.RightMotorModel);
        }

        [Test]
        public void CleanupTest()
        {
            mainWindowViewModel.Cleanup();
            bluetoothClientServiceMock.Verify(o => o.Dispose(), Times.Once());
        }

        private void ProcessDispatcherWorkItemsInBackground()
        {
            var dispatcherFrame = new DispatcherFrame();

            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new DispatcherOperationCallback(state =>
            {
                ((DispatcherFrame)state).Continue = false;
                return null;
            }), dispatcherFrame);

            Dispatcher.PushFrame(dispatcherFrame);
        }
    }
}
