﻿using Moq;
using NUnit.Framework;
using RobotController.Adapters;
using RobotController.Configurations;
using RobotController.Models.Enums;
using RobotController.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Test.Services
{
    [TestFixture]
    class BluetoothClientServiceTest
    {
        private BluetoothClientService bluetoothClientService;
        private Mock<INetworkStreamAdapter> networkStreamMock;
        private Mock<IBluetoothClientAdapter> bluetoothClientMock;
        private Mock<IBluetoothAddressAdapter> bluetoothAddressMock;
        private Mock<IBluetoothClientAdapterFactory> bluetoothClientFactoryMock;
        private Mock<IBluetoothModuleSettings> bluetoothClientSettingsMock;
        private INetworkStreamAdapter networkStream;
        private IBluetoothClientAdapter bluetoothClient;
        private IBluetoothAddressAdapter bluetoothAddress;
        private IBluetoothClientAdapterFactory bluetoothClientFactory;
        private IBluetoothModuleSettings bluetoothClientSettings;
        private IBluetoothDeviceInfoAdapter[] devices = new IBluetoothDeviceInfoAdapter[]
        {
                new Mock<IBluetoothDeviceInfoAdapter>().Object,
                new Mock<IBluetoothDeviceInfoAdapter>().Object
        };

        [SetUp]
        public void Init()
        {
            networkStreamMock = new Mock<INetworkStreamAdapter>();
            networkStreamMock.Setup(o => o.CanRead).Returns(true);
            networkStreamMock.Setup(o => o.CanWrite).Returns(true);
            networkStreamMock.Setup(o => o.Close()).Callback(() =>
            {
                networkStreamMock.Setup(o => o.CanRead).Returns(false);
                networkStreamMock.Setup(o => o.CanWrite).Returns(false);
            });
            networkStream = networkStreamMock.Object;

            bluetoothClientMock = new Mock<IBluetoothClientAdapter>();
            bluetoothClientMock.Setup(o => o.GetStream()).Returns(networkStream);
            bluetoothClientMock.Setup(o => o.DiscoverDevices()).Returns(devices);
            bluetoothClientMock.Setup(o => o.Connected).Returns(true);
            bluetoothClientMock.Setup(o => o.Close()).Callback(() =>
            {
                bluetoothClientMock.Setup(o => o.Connected).Returns(false);
            });
            bluetoothClient = bluetoothClientMock.Object;

            bluetoothAddressMock = new Mock<IBluetoothAddressAdapter>();
            bluetoothAddress = bluetoothAddressMock.Object;

            bluetoothClientFactoryMock = new Mock<IBluetoothClientAdapterFactory>();
            bluetoothClientFactoryMock.Setup(o => o.Create()).Returns(bluetoothClient);
            bluetoothClientFactory = bluetoothClientFactoryMock.Object;

            bluetoothClientSettingsMock = new Mock<IBluetoothModuleSettings>();
            bluetoothClientSettingsMock.Setup(o => o.BluetoothModuleAddress).Returns(bluetoothAddress);
            bluetoothClientSettingsMock.Setup(o => o.BluetoothModulePIN).Returns("1234");
            bluetoothClientSettings = bluetoothClientSettingsMock.Object;

            bluetoothClientService = new BluetoothClientService(bluetoothClient, bluetoothClientSettings);
        }

        [Test]
        public async Task ConnectAndCloseAsyncTest()
        {
            await bluetoothClientService.ConnectAsync();
            INetworkStreamAdapter stream = bluetoothClientService.BluetoothStream;

            Assert.IsTrue(bluetoothClientService.Connected);
            Assert.IsTrue(stream.CanRead && stream.CanWrite);
            Assert.AreSame(networkStream, stream);

            await bluetoothClientService.CloseAsync();

            Assert.IsFalse(bluetoothClientService.Connected);
            Assert.IsFalse(stream.CanRead && stream.CanWrite);

            bluetoothClientMock.Verify(o => o.Connect(bluetoothAddress, BluetoothServiceAdapter.SerialPort), Times.Once());
            bluetoothClientMock.Verify(o => o.GetStream(), Times.Once());
            bluetoothClientMock.Verify(o => o.Close(), Times.Once());
            networkStreamMock.Verify(o => o.Close(), Times.Once());
        }

        [Test]
        public async Task SendControlCommandAsyncTest()
        {
            await bluetoothClientService.ConnectAsync();

            bool result = await bluetoothClientService.SendControlCommandAsync(ControlCommand.FrontDiodeOn);

            networkStreamMock.Verify(o => o.WriteByte(It.Is<byte>(cc => cc == (byte)ControlCommand.FrontDiodeOn)), Times.Once());
            networkStreamMock.Verify(o => o.Flush(), Times.Once());
        }

        [Test]
        public async Task ReadControlCommandAsyncTest()
        {
            await bluetoothClientService.ConnectAsync();

            ControlCommand result = await bluetoothClientService.ReadControlCommandAsync();

            networkStreamMock.Verify(o => o.ReadByte(), Times.Once());
        }

        [Test]
        public async Task GetDiscoveredDevicesAsyncTest()
        {
            BluetoothClientService.BluetoothClientFactory = bluetoothClientFactory;
            List<IBluetoothDeviceInfoAdapter> discoveredDevices = await BluetoothClientService.GetDiscoveredDevices();
            
            Assert.AreEqual(devices.ToList(), discoveredDevices);
            Assert.AreEqual(devices.Length, discoveredDevices.Count);
        }

        [Test]
        public void DisposeBeforeConnectTest()
        {
            bluetoothClientService.Dispose();
            networkStreamMock.Verify(o => o.Dispose(), Times.Never);
            bluetoothClientMock.Verify(o => o.Dispose(), Times.Once());
        }

        [Test]
        public async Task DisposeAfterConnectTest()
        {
            await bluetoothClientService.ConnectAsync();
            bluetoothClientService.Dispose();
            networkStreamMock.Verify(o => o.Dispose(), Times.Once());
            bluetoothClientMock.Verify(o => o.Dispose(), Times.Once());
        }
    }
}
