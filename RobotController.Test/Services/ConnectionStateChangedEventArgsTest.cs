﻿using NUnit.Framework;
using RobotController.Models.Enums;
using RobotController.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Test.Services
{
    [TestFixture]
    class ConnectionStateChangedEventArgsTest
    {
        private ConnectionStateChangedEventArgs connectionStateChangedEventArgs;

        [SetUp]
        public void Init()
        {
            connectionStateChangedEventArgs = new ConnectionStateChangedEventArgs(ConnectionState.Connected);
        }

        [Test]
        public void ConnectionStateTest()
        {
            Assert.AreEqual(ConnectionState.Connected, connectionStateChangedEventArgs.ConnectionState);
        }

    }
}
