﻿using Moq;
using NUnit.Framework;
using RobotController.Adapters;
using RobotController.Configurations;
using RobotController.Models.Enums;
using RobotController.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Test.Services
{
    [TestFixture]
    class BluetoothConnectionSupervisionServiceTest
    {
        private BluetoothConnectionSupervisionService bluetoothConnectionSupervision;

        private Mock<IBluetoothClientService> bluetoothClientServiceMock;
        private Mock<ITimerAdapter> heartbeatTransmitterMock;
        private Mock<ITimerAdapter> heartbeatReceiverMock;
        private Mock<ITimerAdapter> heartbeatTimeCheckerMock;
        private Mock<IBluetoothConnectionSupervisionSettings> bluetoothConnectionSupervisionSettingsMock;
        private IBluetoothClientService bluetoothClientService;
        private ITimerAdapter heartbeatTransmitter;
        private ITimerAdapter heartbeatReceiver;
        private ITimerAdapter heartbeatTimeChecker;
        private IBluetoothConnectionSupervisionSettings bluetoothConnectionSupervisionSettings;

        [SetUp]
        public void Init()
        {
            bluetoothClientServiceMock = new Mock<IBluetoothClientService>();
            bluetoothClientServiceMock.Setup(o => o.Connected).Returns(true);
            bluetoothClientServiceMock.Setup(o => o.SendControlCommandAsync(ControlCommand.Heartbeat)).ReturnsAsync(false);
            bluetoothClientServiceMock.Setup(o => o.ReadControlCommandAsync()).ReturnsAsync(ControlCommand.Heartbeat);
            bluetoothClientService = bluetoothClientServiceMock.Object;

            heartbeatTransmitterMock = new Mock<ITimerAdapter>();
            heartbeatTransmitter = heartbeatTransmitterMock.Object;

            heartbeatReceiverMock = new Mock<ITimerAdapter>();
            heartbeatReceiver = heartbeatReceiverMock.Object;

            heartbeatTimeCheckerMock = new Mock<ITimerAdapter>();
            heartbeatTimeChecker = heartbeatTimeCheckerMock.Object;

            bluetoothConnectionSupervisionSettingsMock = new Mock<IBluetoothConnectionSupervisionSettings>();
            bluetoothConnectionSupervisionSettingsMock.Setup(o => o.ConnectionTimeout).Returns(400);
            bluetoothConnectionSupervisionSettingsMock.Setup(o => o.HeartbeatInterval).Returns(100);
            bluetoothConnectionSupervisionSettings = bluetoothConnectionSupervisionSettingsMock.Object;

            bluetoothConnectionSupervision = new BluetoothConnectionSupervisionService(bluetoothClientService, heartbeatTransmitter, heartbeatReceiver, heartbeatTimeChecker, bluetoothConnectionSupervisionSettings);
        }

        [Test]
        public void StartTest()
        {
            bluetoothConnectionSupervision.Start();

            heartbeatTransmitterMock.Verify(o => o.Start(), Times.Once());
            heartbeatReceiverMock.Verify(o => o.Start(), Times.Once());
            heartbeatTimeCheckerMock.Verify(o => o.Start(), Times.Once());
        }

        [Test]
        public void StopTest()
        {
            bluetoothConnectionSupervision.Stop();

            heartbeatTransmitterMock.Verify(o => o.Stop(), Times.Once());
            heartbeatReceiverMock.Verify(o => o.Stop(), Times.Once());
            heartbeatTimeCheckerMock.Verify(o => o.Stop(), Times.Once());

            Assert.AreEqual(ConnectionState.Disconnected, bluetoothConnectionSupervision.ConnectionState);
        }

        [Test]
        public void ConnectionStateChangedTest()
        {
            bool result = false;
            bluetoothConnectionSupervision.ConnectionStateChanged += (sender, e) =>
            {
                if (e.ConnectionState == ConnectionState.Connected)
                {
                    result = true;
                }
            };

            heartbeatTimeCheckerMock.Raise(o => o.Elapsed += null, new EventArgs() as System.Timers.ElapsedEventArgs);
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void ConnectionStateTest()
        {
            Assert.AreEqual(ConnectionState.Disconnected, bluetoothConnectionSupervision.ConnectionState);
        }

        [Test]
        public void ConnectionTimeoutTest()
        {
            Assert.AreEqual(400, bluetoothConnectionSupervision.ConnectionTimeout);
        }

        [Test]
        public void HeartbeatIntervalTest()
        {
            Assert.AreEqual(100, bluetoothConnectionSupervision.HeartbeatInterval);
        }
    }
}
