﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotController.Models;
using RobotController.Models.Enums;

namespace RobotController.Test.Models
{
    [TestFixture]
    class LightDiodeModelTest
    {
        LightDiodeModel lightDiodeModel;

        [SetUp]
        public void Init()
        {
            lightDiodeModel = new LightDiodeModel();
        }

        [Test]
        public void ToggleTest()
        {
            // By default, light diode activated is off.
            lightDiodeModel.Toggle();
            Assert.AreEqual(true, lightDiodeModel.Activated);

            lightDiodeModel.Toggle();
            Assert.AreEqual(false, lightDiodeModel.Activated);

            lightDiodeModel.Toggle();
            Assert.AreEqual(true, lightDiodeModel.Activated);
        }

        [Test]
        public void ActivatedTest()
        {
            lightDiodeModel.Activated = true;
            Assert.AreEqual(true, lightDiodeModel.Activated);

            lightDiodeModel.Activated = false;
            Assert.AreEqual(false, lightDiodeModel.Activated);
        }

        [Test]
        public void ActivatedPropertyChangedTest()
        {
            bool result = false;
            lightDiodeModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Activated")
                {
                    result = true;
                }
            };

            lightDiodeModel.Activated = false;
            lightDiodeModel.Activated = true;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void ColorTest()
        {
            Assert.NotNull(lightDiodeModel.Color);

            lightDiodeModel.Color = "Blue";
            Assert.AreEqual("Blue", lightDiodeModel.Color);
        }

        [Test]
        public void ColorPropertyChangedTest()
        {
            bool result = false;
            lightDiodeModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Color")
                {
                    result = true;
                }
            };

            lightDiodeModel.Color = String.Empty;
            lightDiodeModel.Color = "New Color";
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void DescriptionTest()
        {
            Assert.NotNull(lightDiodeModel.Description);

            lightDiodeModel.Description = "This LED is located at front of device.";
            Assert.AreEqual("This LED is located at front of device.", lightDiodeModel.Description);
        }

        [Test]
        public void DescriptionPropertyChangedTest()
        {
            bool result = false;
            lightDiodeModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Description")
                {
                    result = true;
                }
            };

            lightDiodeModel.Description = String.Empty;
            lightDiodeModel.Description = "New Description";
            Assert.That(result, Is.EqualTo(true));
        }
    }
}
