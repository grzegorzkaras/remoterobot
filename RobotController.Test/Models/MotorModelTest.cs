﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotController.Models;
using RobotController.Models.Enums;

namespace RobotController.Test.Models
{
    [TestFixture]
    class MotorModelTest
    {
        MotorModel motorModel;

        [SetUp]
        public void Init()
        {
            motorModel = new MotorModel();
        }

        [Test]
        public void SetMotorDirectionTest()
        {
            // By default, motor direction is normal.
            Assert.AreEqual(MotorDirection.None, motorModel.MotorDirection);

            motorModel.MotorDirection = MotorDirection.Forward;
            Assert.AreEqual(MotorDirection.Forward, motorModel.MotorDirection);

            motorModel.MotorDirection = MotorDirection.Backward;
            Assert.AreEqual(MotorDirection.Backward, motorModel.MotorDirection);
        }

        [Test]
        public void MotorDirectionPropertyChangedTest()
        {
            bool result = false;
            motorModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "MotorDirection")
                {
                    result = true;
                }
            };

            motorModel.MotorDirection = MotorDirection.None;
            motorModel.MotorDirection = MotorDirection.Forward;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void DescriptionTest()
        {
            Assert.NotNull(motorModel.Description);

            motorModel.Description = "This Motor is located at left side of device.";
            Assert.AreEqual("This Motor is located at left side of device.", motorModel.Description);
        }

        [Test]
        public void DescriptionPropertyChangedTest()
        {
            bool result = false;
            motorModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Description")
                {
                    result = true;
                }
            };

            motorModel.Description = String.Empty;
            motorModel.Description = "New Description";
            Assert.That(result, Is.EqualTo(true));
        }
    }
}
