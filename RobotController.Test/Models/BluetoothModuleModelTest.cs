﻿using Moq;
using NUnit.Framework;
using RobotController.Adapters;
using RobotController.Models;
using RobotController.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Test.Models
{
    [TestFixture]
    class BluetoothModuleModelTest
    {
        BluetoothModuleModel bluetoothModuleModel;
        IBluetoothDeviceInfoAdapter bluetoothDeviceInfo;

        [SetUp]
        public void Init()
        {
            Mock<IBluetoothDeviceInfoAdapter> bluetoothDeviceInfoMock = new Mock<IBluetoothDeviceInfoAdapter>();
            bluetoothDeviceInfoMock.Setup(o => o.Authenticated).Returns(true);
            bluetoothDeviceInfoMock.Setup(o => o.Connected).Returns(false);
            bluetoothDeviceInfoMock.Setup(o => o.DeviceName).Returns("HC-05");
            bluetoothDeviceInfoMock.Setup(o => o.DeviceAddress).Returns("301410271740");
            bluetoothDeviceInfoMock.Setup(o => o.LastSeen).Returns(new DateTime(2018, 1, 4));
            bluetoothDeviceInfoMock.Setup(o => o.LastUsed).Returns(new DateTime(2018, 1, 3));
            bluetoothDeviceInfoMock.Setup(o => o.Remembered).Returns(true);

            bluetoothDeviceInfo = bluetoothDeviceInfoMock.Object;
            bluetoothModuleModel = new BluetoothModuleModel(bluetoothDeviceInfo);
        }

        [Test]
        public void BluetoothDeviceInfoTest()
        {
            Assert.AreSame(bluetoothDeviceInfo, bluetoothModuleModel.BluetoothDeviceInfo);
        }

        [Test]
        public void BluetoothDeviceInfoPropertyChangedTest()
        {
            bool result = false;
            bluetoothModuleModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "BluetoothDeviceInfo")
                {
                    result = true;
                }
            };

            bluetoothModuleModel.BluetoothDeviceInfo = null;
            bluetoothModuleModel.BluetoothDeviceInfo = bluetoothDeviceInfo;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void AuthenticatedTest()
        {
            Assert.AreEqual(true, bluetoothModuleModel.Authenticated);
        }

        [Test]
        public void AuthenticatedPropertyChangedTest()
        {
            bool result = false;
            bluetoothModuleModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Authenticated")
                {
                    result = true;
                }
            };

            bluetoothModuleModel.Authenticated = false;
            bluetoothModuleModel.Authenticated = true;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void ConnectedTest()
        {
            Assert.AreEqual(ConnectionState.Disconnected, bluetoothModuleModel.Connected);
        }

        [Test]
        public void ConnectedPropertyChangedTest()
        {
            bool result = false;
            bluetoothModuleModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Connected")
                {
                    result = true;
                }
            };

            bluetoothModuleModel.Connected = ConnectionState.Disconnected;
            bluetoothModuleModel.Connected = ConnectionState.Connected;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void DeviceNameTest()
        {
            Assert.AreEqual("HC-05", bluetoothModuleModel.DeviceName);
        }

        [Test]
        public void DeviceNamePropertyChangedTest()
        {
            bool result = false;
            bluetoothModuleModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "DeviceName")
                {
                    result = true;
                }
            };

            bluetoothModuleModel.DeviceName = String.Empty;
            bluetoothModuleModel.DeviceName = "HC-05";
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void DeviceAddressTest()
        {
            Assert.AreEqual("301410271740", bluetoothModuleModel.DeviceAddress);
        }

        [Test]
        public void DeviceAddressPropertyChangedTest()
        {
            bool result = false;
            bluetoothModuleModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "DeviceAddress")
                {
                    result = true;
                }
            };

            bluetoothModuleModel.DeviceAddress = String.Empty;
            bluetoothModuleModel.DeviceAddress = "301410271740";
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void LastSeenTest()
        {
            Assert.AreEqual(new DateTime(2018, 1, 4), bluetoothModuleModel.LastSeen);
        }

        [Test]
        public void LastSeenPropertyChangedTest()
        {
            bool result = false;
            bluetoothModuleModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "LastSeen")
                {
                    result = true;
                }
            };

            bluetoothModuleModel.LastSeen = new DateTime(2018, 1, 4);
            bluetoothModuleModel.LastSeen = new DateTime(2018, 1, 5);
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void LastUsedTest()
        {
            Assert.AreEqual(new DateTime(2018, 1, 3), bluetoothModuleModel.LastUsed);
        }

        [Test]
        public void LastUsedPropertyChangedTest()
        {
            bool result = false;
            bluetoothModuleModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "LastUsed")
                {
                    result = true;
                }
            };

            bluetoothModuleModel.LastUsed = new DateTime(2018, 1, 3);
            bluetoothModuleModel.LastUsed = new DateTime(2018, 1, 4);
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void RememberedTest()
        {
            Assert.AreEqual(true, bluetoothModuleModel.Remembered);
        }

        [Test]
        public void RememberedPropertyChangedTest()
        {
            bool result = false;
            bluetoothModuleModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Remembered")
                {
                    result = true;
                }
            };

            bluetoothModuleModel.Remembered = false;
            bluetoothModuleModel.Remembered = true;
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void NullContructorArgumentExceptionTest()
        {
            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() =>
            {
                BluetoothModuleModel bluetoothModuleModel = new BluetoothModuleModel(null);
            });

            Assert.That(ex.ParamName, Is.EqualTo("bluetoothDeviceInfo"));
        }
    }
}
