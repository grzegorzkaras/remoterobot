using CommonServiceLocator.StructureMapAdapter.Unofficial;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using RobotController.Registers;
using RobotController.ViewModels;
using StructureMap;
using System.Reflection;

namespace RobotController.ViewModelLocator
{
    public class StructureMapViewModelLocator
    {
        public StructureMapViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => CreateServiceLocator());
        }

        private static IServiceLocator CreateServiceLocator()
        {
            IContainer container = Container.For<MainRegistry>();
            return new StructureMapServiceLocator(container);
        }

        public MainWindowViewModel MainWindow => ServiceLocator.Current.GetInstance<MainWindowViewModel>();

        public static void Cleanup()
        {
            ServiceLocator.Current.GetInstance<MainWindowViewModel>().Cleanup();
            SimpleIoc.Default.Reset();
        }
    }
}