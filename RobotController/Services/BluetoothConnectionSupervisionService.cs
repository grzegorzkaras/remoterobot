﻿using RobotController.Adapters;
using RobotController.Configurations;
using RobotController.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RobotController.Services
{
    public delegate void ConnectionStateChangedEventHandler(object sender, ConnectionStateChangedEventArgs e);

    public class ConnectionStateChangedEventArgs : EventArgs
    {
        public ConnectionState ConnectionState { get; private set; }

        public ConnectionStateChangedEventArgs(ConnectionState connectionState)
        {
            ConnectionState = connectionState;
        }
    }

    public sealed class BluetoothConnectionSupervisionService : IBluetoothConnectionSupervisionService, IDisposable
    {
        private bool disposed;

        private volatile bool stopRequest = false;
        private DateTime lastReceivedHeartbeatTime = DateTime.Now.ToUniversalTime();

        private ITimerAdapter heartbeatTimeChecker;
        private ITimerAdapter heartbeatReceiver;
        private ITimerAdapter heartbeatTransmitter;
        private IBluetoothClientService bluetoothClientService;
        
        private ConnectionState connectionState = ConnectionState.Disconnected;
        public ConnectionState ConnectionState {
            get { return connectionState; }
            private set
            {
                if (connectionState != value)
                {
                    connectionState = value;
                    OnConnectionStateChanged(new ConnectionStateChangedEventArgs(value));
                }
            }
        }
        public event ConnectionStateChangedEventHandler ConnectionStateChanged;

        public double ConnectionTimeout { get; private set; }
        public double HeartbeatInterval { get; private set; }

        public BluetoothConnectionSupervisionService(IBluetoothClientService bluetoothClientService, ITimerAdapter heartbeatTransmitter, ITimerAdapter heartbeatReceiver, ITimerAdapter heartbeatTimeChecker, IBluetoothConnectionSupervisionSettings bluetoothConnectionSupervisionSettings)
        {
            this.bluetoothClientService = bluetoothClientService;

            ConnectionTimeout = bluetoothConnectionSupervisionSettings.ConnectionTimeout;
            HeartbeatInterval = bluetoothConnectionSupervisionSettings.HeartbeatInterval;

            this.heartbeatTransmitter = heartbeatTransmitter;
            this.heartbeatTransmitter.Interval = HeartbeatInterval;
            this.heartbeatTransmitter.Enabled = true;
            this.heartbeatTransmitter.Elapsed += HeartbeatTransmitter_Elapsed;

            this.heartbeatReceiver = heartbeatReceiver;
            this.heartbeatReceiver.Interval = HeartbeatInterval;
            this.heartbeatReceiver.Enabled = true;
            this.heartbeatReceiver.Elapsed += HeartbeatReceiver_Elapsed;

            this.heartbeatTimeChecker = heartbeatTimeChecker;
            this.heartbeatTimeChecker.Interval = HeartbeatInterval;
            this.heartbeatTimeChecker.Enabled = true;
            this.heartbeatTimeChecker.Elapsed += HeartbeatTimeChecker_Elapsed;
        }

        private void OnConnectionStateChanged(ConnectionStateChangedEventArgs e)
        {
            ConnectionStateChanged?.Invoke(this, e);
        }

        private async void HeartbeatTransmitter_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (stopRequest)
            {
                return;
            }

            if (bluetoothClientService.Connected)
            {
                await bluetoothClientService.SendControlCommandAsync(ControlCommand.Heartbeat);
            }
        }

        private async void HeartbeatReceiver_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (stopRequest)
            {
                return;
            }

            if (bluetoothClientService.Connected)
            {
                ControlCommand heartbeat = await bluetoothClientService.ReadControlCommandAsync();
                if (heartbeat == ControlCommand.Heartbeat)
                {
                    lastReceivedHeartbeatTime = DateTime.Now.ToUniversalTime();
                }
            }
        }

        private void HeartbeatTimeChecker_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (stopRequest)
            {
                return;
            }

            if (bluetoothClientService.Connected)
            {
                TimeSpan timeSinceLastHeartbeat = DateTime.Now.ToUniversalTime() - lastReceivedHeartbeatTime;

                if (timeSinceLastHeartbeat > TimeSpan.FromMilliseconds(ConnectionTimeout))
                {
                    ConnectionState = ConnectionState.Disconnected;
                }
                else
                {
                    ConnectionState = ConnectionState.Connected;
                }
            }
        }

        public void Start()
        {
            stopRequest = false;
            heartbeatTransmitter.Start();
            heartbeatReceiver.Start();
            heartbeatTimeChecker.Start();
        }

        public void Stop()
        {
            stopRequest = true;
            heartbeatTransmitter.Stop();
            heartbeatReceiver.Stop();
            heartbeatTimeChecker.Stop();
            ConnectionState = ConnectionState.Disconnected;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                Stop();
                heartbeatTimeChecker.Dispose();
                heartbeatReceiver.Dispose();
                heartbeatTransmitter.Dispose();
                bluetoothClientService.Dispose();
            }

            heartbeatTimeChecker = null;
            heartbeatReceiver = null;
            heartbeatTransmitter = null;
            bluetoothClientService = null;
            ConnectionState = ConnectionState.Disconnected;

            disposed = true;
        }
    }
}
