﻿using System.Threading.Tasks;
using RobotController.Adapters;
using RobotController.Models.Enums;
using System;

namespace RobotController.Services
{
    public interface IBluetoothClientService : IDisposable
    {
        INetworkStreamAdapter BluetoothStream { get; }
        bool Connected { get; }

        Task CloseAsync();
        Task ConnectAsync();
        Task<ControlCommand> ReadControlCommandAsync();
        Task<bool> SendControlCommandAsync(ControlCommand command);
    }
}