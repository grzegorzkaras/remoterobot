﻿using RobotController.Models.Enums;
using System;

namespace RobotController.Services
{
    public interface IBluetoothConnectionSupervisionService
    {
        ConnectionState ConnectionState { get; }
        double ConnectionTimeout { get; }
        double HeartbeatInterval { get; }

        event ConnectionStateChangedEventHandler ConnectionStateChanged;

        void Start();
        void Stop();
    }
}