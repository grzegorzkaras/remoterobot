﻿using GalaSoft.MvvmLight;
using NLog;
using RobotController.Adapters;
using RobotController.Configurations;
using RobotController.Models.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RobotController.Services
{
    public sealed class BluetoothClientService : IBluetoothClientService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static IBluetoothClientAdapterFactory BluetoothClientFactory { private get; set; } = new BluetoothClientAdapterFactory();
        public bool Connected => bluetoothClient != null ? bluetoothClient.Connected : false;
        public INetworkStreamAdapter BluetoothStream { get; private set; } = null;
        private bool disposed = false;
        private IBluetoothClientAdapter bluetoothClient;
        private IBluetoothAddressAdapter bluetoothModuleAddress;

        public BluetoothClientService(IBluetoothClientAdapter bluetoothClient, IBluetoothModuleSettings bluetoothClientSettings)
        {
            this.bluetoothClient = bluetoothClient;
            this.bluetoothModuleAddress = bluetoothClientSettings.BluetoothModuleAddress;

            string pin = bluetoothClientSettings.BluetoothModulePIN;
            this.bluetoothClient.SetPin(pin);
        }

        public static async Task<List<IBluetoothDeviceInfoAdapter>> GetDiscoveredDevices()
        {
            Task<List<IBluetoothDeviceInfoAdapter>> task = Task.Run(() =>
            {
                using (IBluetoothClientAdapter bluetoothClient = BluetoothClientFactory.Create())
                {
                    List<IBluetoothDeviceInfoAdapter> discoveredDevices = bluetoothClient.DiscoverDevices().ToList();
                    return discoveredDevices;
                }
            });

            return await task;
        }

        public async Task ConnectAsync()
        {
            Task task = Task.Run(() =>
            {
                try
                {
                    bluetoothClient.Connect(bluetoothModuleAddress, BluetoothServiceAdapter.SerialPort);
                    BluetoothStream = bluetoothClient.GetStream();
                }
                catch (Exception ex)
                {
                    logger.Debug(ex.Message);
                }
            });

            await task;
        }

        public async Task<bool> SendControlCommandAsync(ControlCommand command)
        {
            Task<bool> task = Task.Run(() =>
            {
                try
                {
                    if (Connected && BluetoothStream != null)
                    {
                        if (BluetoothStream.CanWrite)
                        {
                            BluetoothStream.WriteByte((byte)command);
                            BluetoothStream.Flush();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    logger.Debug(ex.Message);
                    return false;
                }
            });

            return await task;
        }

        public async Task<ControlCommand> ReadControlCommandAsync()
        {
            Task<ControlCommand> task = Task.Run<ControlCommand>(() =>
            {
                try
                {
                    if (Connected && BluetoothStream != null)
                    {
                        if (BluetoothStream.CanRead)
                        {
                            return (ControlCommand)BluetoothStream.ReadByte();
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Debug(ex.Message);
                    return ControlCommand.None;
                }

                return ControlCommand.None;
            });

            return await task;
        }

        public async Task CloseAsync()
        {
            Task task = Task.Run(() => 
            {
                if (BluetoothStream != null)
                {
                    BluetoothStream.Close();
                }

                bluetoothClient.Close();
            });

            await task;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                bluetoothClient.Dispose();

                if (BluetoothStream != null)
                    BluetoothStream.Dispose();
            }

            bluetoothClient = null;
            BluetoothStream = null;

            disposed = true;
        }
    }
}
