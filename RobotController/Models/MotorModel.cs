﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotController.Models.Enums;
using GalaSoft.MvvmLight;

namespace RobotController.Models
{
    public class MotorModel : ObservableObject, IMotorModel
    {
        private MotorDirection motorDirection = MotorDirection.None;
        public MotorDirection MotorDirection
        {
            get => motorDirection;
            set => Set(ref motorDirection, value);
        }

        private String description = String.Empty;
        public String Description
        {
            get => description;
            set => Set(ref description, value);
        }
    }
}
