﻿namespace RobotController.Models
{
    public interface ILightDiodeModel
    {
        bool Activated { get; set; }
        string Color { get; set; }
        string Description { get; set; }

        void Toggle();
    }
}