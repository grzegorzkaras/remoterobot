﻿using RobotController.Models.Enums;

namespace RobotController.Models
{
    public interface IMotorModel
    {
        string Description { get; set; }
        MotorDirection MotorDirection { get; set; }
    }
}