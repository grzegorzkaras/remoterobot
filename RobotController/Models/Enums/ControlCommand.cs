﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Models.Enums
{
    public enum ControlCommand : byte
    {
        None,
        Heartbeat,
        DriveForward,
        DriveBackward,
        DriveLeft,
        DriveRight,
        FrontDiodeOn,
        FrontDiodeOff,
        StopMotors
    }
}