﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Models.Enums
{
    public enum ConnectionState
    {
        Disconnected,
        Connected
    }
}
