﻿using GalaSoft.MvvmLight;
using RobotController.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Models
{
    public sealed class LightDiodeModel : ObservableObject, ILightDiodeModel
    {
        private bool activated = false;
        public bool Activated
        {
            get => activated;
            set => Set(ref activated, value);
        }

        private String color = String.Empty;
        public String Color
        {
            get => color;
            set => Set(ref color, value);
        }

        private String description = String.Empty;
        public String Description
        {
            get => description;
            set => Set(ref description, value);
        }

        public void Toggle()
        {
            Activated = !Activated;
        }
    }
}
