﻿using System;
using RobotController.Adapters;
using RobotController.Models.Enums;

namespace RobotController.Models
{
    public interface IBluetoothModuleModel
    {
        bool Authenticated { get; set; }
        IBluetoothDeviceInfoAdapter BluetoothDeviceInfo { get; set; }
        ConnectionState Connected { get; set; }
        string DeviceAddress { get; set; }
        string DeviceName { get; set; }
        DateTime LastSeen { get; set; }
        DateTime LastUsed { get; set; }
        bool Remembered { get; set; }
    }
}