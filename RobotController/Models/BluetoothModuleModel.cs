﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InTheHand.Net.Sockets;
using RobotController.Adapters;
using GalaSoft.MvvmLight;
using RobotController.Models.Enums;

namespace RobotController.Models
{
    public class BluetoothModuleModel : ObservableObject, IBluetoothModuleModel
    {
        private IBluetoothDeviceInfoAdapter bluetothDeviceInfo;
        public IBluetoothDeviceInfoAdapter BluetoothDeviceInfo
        {
            get => bluetothDeviceInfo;
            set => Set(ref bluetothDeviceInfo, value);
        }

        private bool authenticated;
        public bool Authenticated
        {
            get => authenticated;
            set => Set(ref authenticated, value);
        }

        private ConnectionState connected;
        public ConnectionState Connected
        {
            get => connected;
            set => Set(ref connected, value);
        }

        private String deviceName;
        public String DeviceName
        {
            get => deviceName;
            set => Set(ref deviceName, value);
        }

        private String deviceAddress;
        public String DeviceAddress
        {
            get => deviceAddress;
            set => Set(ref deviceAddress, value);
        }

        private DateTime lastSeen;
        public DateTime LastSeen
        {
            get => lastSeen;
            set => Set(ref lastSeen, value);
        }

        private DateTime lastUsed;
        public DateTime LastUsed
        {
            get => lastUsed;
            set => Set(ref lastUsed, value);
        }

        private bool remembered;
        public bool Remembered
        {
            get => remembered;
            set => Set(ref remembered, value);
        }

        public BluetoothModuleModel(IBluetoothDeviceInfoAdapter bluetoothDeviceInfo)
        {
            BluetoothDeviceInfo = bluetoothDeviceInfo ?? throw new ArgumentNullException(nameof(bluetoothDeviceInfo));
            Authenticated = bluetoothDeviceInfo.Authenticated;
            Connected = bluetoothDeviceInfo.Connected ? ConnectionState.Connected : ConnectionState.Disconnected;
            DeviceName = bluetoothDeviceInfo.DeviceName;
            DeviceAddress = bluetoothDeviceInfo.DeviceAddress;
            LastSeen = bluetoothDeviceInfo.LastSeen;
            LastUsed = bluetoothDeviceInfo.LastUsed;
            Remembered = bluetoothDeviceInfo.Remembered;
        }
    }
}
