using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using NLog;
using RobotController.Models;
using RobotController.Models.Enums;
using RobotController.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RobotController.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private Logger logger = LogManager.GetCurrentClassLogger();
        private bool connected = false;
        private Key? pressedKey = null; 

        private IBluetoothClientService bluetoothClientService;
        private IBluetoothConnectionSupervisionService bluetoothConnectionSupervisionService;

        public IBluetoothModuleModel BluetoothModuleModel { get; private set; }
        public ILightDiodeModel FrontDiodeModel { get; private set; }
        public IMotorModel LeftMotorModel { get; private set; }
        public IMotorModel RightMotorModel { get; private set; }

        public RelayCommand WindowLoadedCommand { get; private set; }
        public RelayCommand WindowClosingCommand { get; private set; }
        public RelayCommand<KeyEventArgs> WindowKeyDownCommand { get; private set; }
        public RelayCommand<KeyEventArgs> WindowKeyUpCommand { get; private set; }

        public RelayCommand LKeyButtonPressCommand { get; private set; }
        public RelayCommand LeftKeyButtonPressCommand { get; private set; }
        public RelayCommand RightKeyButtonPressCommand { get; private set; }
        public RelayCommand UpKeyButtonPressCommand { get; private set; }
        public RelayCommand DownKeyButtonPressCommand { get; private set; }

        private bool lKeyChecked;
        public bool LKeyChecked
        {
            get => lKeyChecked;
            set => Set(ref lKeyChecked, value);
        }

        private bool leftKeyChecked;
        public bool LeftKeyChecked
        {
            get => leftKeyChecked;
            set => Set(ref leftKeyChecked, value);
        }

        private bool rightKeyChecked;
        public bool RightKeyChecked
        {
            get => rightKeyChecked;
            set => Set(ref rightKeyChecked, value);
        }

        private bool upKeyChecked;
        public bool UpKeyChecked
        {
            get => upKeyChecked;
            set => Set(ref upKeyChecked, value);
        }

        private bool downKeyChecked;
        public bool DownKeyChecked
        {
            get => downKeyChecked;
            set => Set(ref downKeyChecked, value);
        }

        public MainWindowViewModel(IBluetoothClientService bluetoothClientService, IBluetoothConnectionSupervisionService bluetoothConnectionSupervisionService, 
            IBluetoothModuleModel bluetoothModuleModel, ILightDiodeModel frontLightDiodeModel, IMotorModel leftMotorModel, IMotorModel rightMotorModel)
        {
            this.bluetoothClientService = bluetoothClientService;
            this.bluetoothConnectionSupervisionService = bluetoothConnectionSupervisionService;
            this.bluetoothConnectionSupervisionService.ConnectionStateChanged += BluetoothConnectionSupervisionService_ConnectionStateChanged;

            BluetoothModuleModel = bluetoothModuleModel;
            FrontDiodeModel = frontLightDiodeModel;
            LeftMotorModel = leftMotorModel;
            RightMotorModel = rightMotorModel;

            WindowLoadedCommand = new RelayCommand(BluetoothConnectionConnect);
            WindowClosingCommand = new RelayCommand(BluetoothConnectionClose);
            WindowKeyDownCommand = new RelayCommand<KeyEventArgs>(SelectKeyDownAction, (e) => connected);
            WindowKeyUpCommand = new RelayCommand<KeyEventArgs>(SelectKeyUpAction, (e) => connected);

            LKeyButtonPressCommand = new RelayCommand(SelectLKeyButtonPress, () => connected);
            LeftKeyButtonPressCommand = new RelayCommand(SelectLeftKeyButtonPress, () => connected);
            RightKeyButtonPressCommand = new RelayCommand(SelectRightKeyButtonPress, () => connected);
            UpKeyButtonPressCommand = new RelayCommand(SelectUpKeyButtonPress, () => connected);
            DownKeyButtonPressCommand = new RelayCommand(SelectDownKeyButtonPress, () => connected);
        }

        private async void BluetoothConnectionConnect()
        {
            await bluetoothClientService.ConnectAsync();
            bool connectedStatus = bluetoothClientService.Connected;
            if (connectedStatus)
            {
                BluetoothModuleModel.Connected = ConnectionState.Connected;
                connected = true;
                bluetoothConnectionSupervisionService.Start();
                CommandManager.InvalidateRequerySuggested();
            }
        }

        private async void BluetoothConnectionClose()
        {
            await bluetoothClientService.CloseAsync();
            bluetoothConnectionSupervisionService.Stop();
        }

        private async void SelectKeyDownAction(KeyEventArgs e)
        {
            if (pressedKey == null)
            {
                pressedKey = e.Key;

                LeftKeyChecked = false;
                RightKeyChecked = false;
                UpKeyChecked = false;
                DownKeyChecked = false;

                switch (e.Key)
                {
                    case Key.L:
                        await SwitchFrontLightDiode(!FrontDiodeModel.Activated);
                        LKeyChecked = FrontDiodeModel.Activated;
                        break;
                    case Key.Left:
                        await DriveLeft();
                        LeftKeyChecked = true;
                        break;
                    case Key.Right:
                        await DriveRight();
                        RightKeyChecked = true;
                        break;
                    case Key.Up:
                        await DriveForward();
                        UpKeyChecked = true;
                        break;
                    case Key.Down:
                        await DriveBackward();
                        DownKeyChecked = true;
                        break;
                }
            }
        }

        private async void SelectKeyUpAction(KeyEventArgs e)
        {
            if (pressedKey == e.Key)
            {
                pressedKey = null;

                switch (e.Key)
                {
                    case Key.Left:
                        await StopMotors();
                        LeftKeyChecked = false;
                        break;
                    case Key.Right:
                        await StopMotors();
                        RightKeyChecked = false;
                        break;
                    case Key.Up:
                        await StopMotors();
                        UpKeyChecked = false;
                        break;
                    case Key.Down:
                        await StopMotors();
                        DownKeyChecked = false;
                        break;
                }
            }
        }

        private async void SelectLKeyButtonPress()
        {
            if (LKeyChecked)
            {
                await ActivateFrontLightDiode();
            }
            else
            {
                await DeactivateFrontLightDiode();
            }
        }

        private async void SelectLeftKeyButtonPress()
        {
            if (LeftKeyChecked)
            {
                RightKeyChecked = false;
                UpKeyChecked = false;
                DownKeyChecked = false;
                await DriveLeft();
            }
            else
            {
                await StopMotors();
            }
        }

        private async void SelectRightKeyButtonPress()
        {
            if (RightKeyChecked)
            {
                LeftKeyChecked = false;
                UpKeyChecked = false;
                DownKeyChecked = false;
                await DriveRight();
            }
            else
            {
                await StopMotors();
            }
        }

        private async void SelectUpKeyButtonPress()
        {
            if (UpKeyChecked)
            {
                LeftKeyChecked = false;
                RightKeyChecked = false;
                DownKeyChecked = false;
                await DriveForward();
            }
            else
            {
                await StopMotors();
            }
        }

        private async void SelectDownKeyButtonPress()
        {
            if (DownKeyChecked)
            {
                LeftKeyChecked = false;
                RightKeyChecked = false;
                UpKeyChecked = false;
                await DriveBackward();
            }
            else
            {
                await StopMotors();
            }
        }

        private void BluetoothConnectionSupervisionService_ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            bool connectedStatus = e.ConnectionState == ConnectionState.Connected ? true : false;

            if (connectedStatus != connected)
            {
                connected = connectedStatus;
                BluetoothModuleModel.Connected = e.ConnectionState;
                CommandManager.InvalidateRequerySuggested();
            }
            
            if (connectedStatus == false)
            {
                LKeyChecked = false;
                LeftKeyChecked = false;
                RightKeyChecked = false;
                UpKeyChecked = false;
                DownKeyChecked = false;
            }
        }
        
        private async Task SwitchFrontLightDiode(bool activate)
        {
            if (activate)
            {
                await ActivateFrontLightDiode();
            }
            else
            {
                await DeactivateFrontLightDiode();
            }
        }

        private async Task ActivateFrontLightDiode()
        {
            bool success = await bluetoothClientService.SendControlCommandAsync(ControlCommand.FrontDiodeOn);
            if (success)
            {
                FrontDiodeModel.Activated = true;
            }
        }

        private async Task DeactivateFrontLightDiode()
        {
            bool success = await bluetoothClientService.SendControlCommandAsync(ControlCommand.FrontDiodeOff);
            if (success)
            {
                FrontDiodeModel.Activated = false;
            }
        }

        private async Task DriveLeft()
        {
            bool success = await bluetoothClientService.SendControlCommandAsync(ControlCommand.DriveLeft);
            if (success)
            {
                LeftMotorModel.MotorDirection = MotorDirection.Backward;
                RightMotorModel.MotorDirection = MotorDirection.Forward;
            }
        }

        private async Task DriveRight()
        {
            bool success = await bluetoothClientService.SendControlCommandAsync(ControlCommand.DriveRight);
            if (success)
            {
                LeftMotorModel.MotorDirection = MotorDirection.Forward;
                RightMotorModel.MotorDirection = MotorDirection.Backward;
            }
        }

        private async Task DriveForward()
        {
            bool success = await bluetoothClientService.SendControlCommandAsync(ControlCommand.DriveForward);
            if (success)
            {
                LeftMotorModel.MotorDirection = MotorDirection.Forward;
                RightMotorModel.MotorDirection = MotorDirection.Forward;
            }
        }

        private async Task DriveBackward()
        {
            bool success = await bluetoothClientService.SendControlCommandAsync(ControlCommand.DriveBackward);
            if (success)
            {
                LeftMotorModel.MotorDirection = MotorDirection.Backward;
                RightMotorModel.MotorDirection = MotorDirection.Backward;
            }
        }

        private async Task StopMotors()
        {
            bool success = await bluetoothClientService.SendControlCommandAsync(ControlCommand.StopMotors);
            if (success)
            {
                LeftMotorModel.MotorDirection = MotorDirection.None;
                RightMotorModel.MotorDirection = MotorDirection.None;
            }
        }
        
        public override void Cleanup()
        {
            bluetoothClientService.Dispose();
            base.Cleanup();
        }
    }
}