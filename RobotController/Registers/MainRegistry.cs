﻿using RobotController.Adapters;
using RobotController.Configurations;
using RobotController.Models;
using RobotController.Services;
using RobotController.ViewModels;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Registers
{
    public class MainRegistry : Registry
    {
        public MainRegistry()
        {
            For<IBluetoothModuleSettings>().Use<BluetoothModuleSettings>();
            For<IBluetoothConnectionSupervisionSettings>().Use<BluetoothConnectionSupervisionSettings>();

            For<IBluetoothAddressAdapter>().Use<BluetoothAddressAdapter>();
            For<IBluetoothClientAdapter>().Use<BluetoothClientAdapter>();
            For<IBluetoothClientAdapterFactory>().Use<BluetoothClientAdapterFactory>();
            For<IBluetoothDeviceInfoAdapter>().Use<BluetoothDeviceInfoAdapter>();
            For<INetworkStreamAdapter>().Use<NetworkStreamAdapter>();
            For<ITimerAdapter>().Use<TimerAdapter>();

            For<IBluetoothClientService>().Use<BluetoothClientService>();
            For<IBluetoothConnectionSupervisionService>().Use<BluetoothConnectionSupervisionService>();

            For<IBluetoothDeviceInfoAdapter>().Use<BluetoothDeviceInfoAdapter>().Ctor<IBluetoothAddressAdapter>().Is(new BluetoothModuleSettings().BluetoothModuleAddress).Named("BluetoothModuleModelDeviceInfo");

            For<IBluetoothModuleModel>().Use<BluetoothModuleModel>().Ctor<IBluetoothDeviceInfoAdapter>().IsNamedInstance("BluetoothModuleModelDeviceInfo").Named("BluetoothModuleModel");
            For<ILightDiodeModel>().Use<LightDiodeModel>().Setter(c => c.Color).Is("Green.").Setter(c => c.Description).Is("Front LED.").Named("FrontLightDiodeModel");
            For<IMotorModel>().Use<MotorModel>().Setter(c => c.Description).Is("Left motor.").Named("LeftMotorModel");
            For<IMotorModel>().Use<MotorModel>().Setter(c => c.Description).Is("Right motor.").Named("RightMotorModel");

            ForConcreteType<MainWindowViewModel>().Configure
                .Ctor<IBluetoothModuleModel>("bluetoothModuleModel").IsNamedInstance("BluetoothModuleModel")
                .Ctor<ILightDiodeModel>("frontLightDiodeModel").IsNamedInstance("FrontLightDiodeModel")
                .Ctor<IMotorModel>("leftMotorModel").IsNamedInstance("LeftMotorModel")
                .Ctor<IMotorModel>("rightMotorModel").IsNamedInstance("RightMotorModel");
        }
    }
}
