﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Configurations
{
    public interface IBluetoothConnectionSupervisionSettings
    {
        double ConnectionTimeout { get; set; }
        double HeartbeatInterval { get; set; }
    }
}
