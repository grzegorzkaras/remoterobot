﻿using RobotController.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Configurations
{
    public class BluetoothModuleSettings : IBluetoothModuleSettings
    {
        public string BluetoothModulePIN { get; set; } = Properties.Settings.Default.BluetoothModulePIN;
        public IBluetoothAddressAdapter BluetoothModuleAddress { get; set; } = new BluetoothAddressAdapter(Properties.Settings.Default.BluetoothModuleAddress);
    }
}
