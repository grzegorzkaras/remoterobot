﻿using RobotController.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Configurations
{
    public interface IBluetoothModuleSettings
    {
        string BluetoothModulePIN { get; set; }
        IBluetoothAddressAdapter BluetoothModuleAddress { get; set; }
    }
}
