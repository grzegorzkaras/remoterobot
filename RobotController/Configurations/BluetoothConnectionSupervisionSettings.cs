﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Configurations
{
    public class BluetoothConnectionSupervisionSettings : IBluetoothConnectionSupervisionSettings
    {
        public double ConnectionTimeout { get; set; } = Properties.Settings.Default.ConnectionTimeout;
        public double HeartbeatInterval { get; set; } = Properties.Settings.Default.HeartbeatInterval;
    }
}
