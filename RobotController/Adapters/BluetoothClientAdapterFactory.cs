﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Adapters
{
    public class BluetoothClientAdapterFactory : IBluetoothClientAdapterFactory
    {
        public IBluetoothClientAdapter Create() => new BluetoothClientAdapter();
    }
}
