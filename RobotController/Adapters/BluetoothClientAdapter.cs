﻿using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Adapters
{
    public class BluetoothClientAdapter : IBluetoothClientAdapter
    {
        private BluetoothClient adaptee = new BluetoothClient();
        private bool disposed = false;

        public bool Connected => adaptee.Connected;

        public void Connect(IBluetoothAddressAdapter bluetoothAddress, Guid serviceClass)
        {
            adaptee.Connect(bluetoothAddress.Adaptee, serviceClass);
        }

        public void Close()
        {
            adaptee.Close();
        }

        public IBluetoothDeviceInfoAdapter[] DiscoverDevices()
        {
            BluetoothDeviceInfo[] devices = adaptee.DiscoverDevices();
            IBluetoothDeviceInfoAdapter[] modules = Array.ConvertAll(devices, device => new BluetoothDeviceInfoAdapter(device));
            return modules;
        }

        public INetworkStreamAdapter GetStream()
        {
            return new NetworkStreamAdapter(adaptee.GetStream());
        }

        public void SetPin(string pin)
        {
            adaptee.SetPin(pin);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                adaptee.Dispose();
            }

            adaptee = null;

            disposed = true;
        }
    }
}
