﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InTheHand.Net;

namespace RobotController.Adapters
{
    public class BluetoothAddressAdapter : IBluetoothAddressAdapter
    {
        private BluetoothAddress adaptee;
        public BluetoothAddress Adaptee => adaptee;

        public BluetoothAddressAdapter(string address)
        {
            adaptee = BluetoothAddress.Parse(address);
        }
    }
}
