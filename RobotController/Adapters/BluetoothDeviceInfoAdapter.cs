﻿using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Adapters
{
    public class BluetoothDeviceInfoAdapter : IBluetoothDeviceInfoAdapter
    {
        private BluetoothDeviceInfo adaptee = null;

        public bool Authenticated => adaptee.Authenticated;

        public bool Connected => adaptee.Connected;

        public string DeviceName => adaptee.DeviceName;

        public string DeviceAddress => adaptee.DeviceAddress.ToString();

        public DateTime LastSeen => adaptee.LastSeen;

        public DateTime LastUsed => adaptee.LastUsed;

        public bool Remembered => adaptee.Remembered;

        public BluetoothDeviceInfoAdapter(IBluetoothAddressAdapter address)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address));

            this.adaptee = new BluetoothDeviceInfo(address.Adaptee);
        }

        public BluetoothDeviceInfoAdapter(BluetoothDeviceInfo adaptee)
        {
            this.adaptee = adaptee ?? throw new ArgumentNullException(nameof(adaptee));
        }
    }
}
