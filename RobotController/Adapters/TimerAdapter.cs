﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RobotController.Adapters
{
    public class TimerAdapter : ITimerAdapter
    {
        private Timer adaptee = new Timer();

        public event ElapsedEventHandler Elapsed
        {
            add { adaptee.Elapsed += value; }
            remove { adaptee.Elapsed -= value; }
        }

        public double Interval
        {
            get => adaptee.Interval;
            set => adaptee.Interval = value;
        }

        public bool Enabled
        {
            get => adaptee.Enabled;
            set => adaptee.Enabled = value;
        }

        public void Start()
        {
            adaptee.Start();
        }

        public void Stop()
        {
            adaptee.Stop();
        }

        public void Dispose()
        {
            adaptee.Dispose();
        }
    }
}
