﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Adapters
{
    public interface IBluetoothClientAdapter : IDisposable
    {
        bool Connected { get; }

        INetworkStreamAdapter GetStream();
        IBluetoothDeviceInfoAdapter[] DiscoverDevices();
        void SetPin(string pin);
        void Connect(IBluetoothAddressAdapter bluetoothAddress, Guid serviceClass);
        void Close();
    }
}
