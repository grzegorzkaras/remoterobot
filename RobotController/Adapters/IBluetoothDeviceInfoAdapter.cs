﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Adapters
{
    public interface IBluetoothDeviceInfoAdapter
    {
        bool Authenticated { get; }
        bool Connected { get; }
        string DeviceName { get; }
        string DeviceAddress { get; }
        DateTime LastSeen { get; }
        DateTime LastUsed { get; }
        bool Remembered { get; }
    }
}
