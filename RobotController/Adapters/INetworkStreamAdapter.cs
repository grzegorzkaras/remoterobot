﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Adapters
{
    public interface INetworkStreamAdapter
    {
        bool CanRead { get; }
        bool CanWrite { get; }

        void WriteByte(byte value);
        int ReadByte();
        void Flush();
        IAsyncResult BeginRead(byte[] buffer, int offset, int size, AsyncCallback callback, object state);
        int EndRead(IAsyncResult asyncResult);
        void Close();
        void Dispose();
    }
}
