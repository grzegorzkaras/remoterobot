﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Adapters
{
    public class NetworkStreamAdapter : INetworkStreamAdapter
    {
        private NetworkStream adaptee = null;

        public bool CanRead => adaptee.CanRead;
        public bool CanWrite => adaptee.CanWrite;

        public NetworkStreamAdapter(NetworkStream adaptee)
        {
            this.adaptee = adaptee;
        }

        public IAsyncResult BeginRead(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
        {
            return adaptee.BeginRead(buffer, offset, size, callback, state);
        }

        public void Close()
        {
            adaptee.Close();
        }

        public void Dispose()
        {
            adaptee.Dispose();
        }

        public int EndRead(IAsyncResult asyncResult)
        {
            return adaptee.EndRead(asyncResult);
        }

        public void Flush()
        {
            adaptee.Flush();
        }

        public void WriteByte(byte value)
        {
            adaptee.WriteByte(value);
        }

        public int ReadByte()
        {
            return adaptee.ReadByte();
        }
    }
}
