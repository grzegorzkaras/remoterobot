﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InTheHand.Net;

namespace RobotController.Adapters
{
    public interface IBluetoothAddressAdapter
    {
        BluetoothAddress Adaptee { get; }
    }
}
