﻿using InTheHand.Net.Bluetooth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Adapters
{
    public static class BluetoothServiceAdapter
    {
        public static Guid SerialPort => BluetoothService.SerialPort;
    }
}
