#define FRONT_DIODE_PIN 8

#define LEFT_MOTOR_ENABLE_PIN 5
#define LEFT_MOTOR_INPUT_ONE_PIN 2
#define LEFT_MOTOR_INPUT_TWO_PIN 3

#define RIGHT_MOTOR_ENABLE_PIN 6
#define RIGHT_MOTOR_INPUT_ONE_PIN 10
#define RIGHT_MOTOR_INPUT_TWO_PIN 9

#define CONNECTION_TIMEOUT 400
#define HEARTBEAT_INTERVAL 100

unsigned long lastHeartbeatTransmitTime = 0;
unsigned long lastHeartbeatReceiveTime = 0;

boolean firstHeartbeatInConnectionReceived = false;
boolean connected = false;

boolean frontDiodeActivated = false;
boolean leftMotorActivated = false;
boolean rightMotorActivated = false;

struct ControlCodes {
  const static byte None = 0x00;
  const static byte Heartbeat = 0x01;
  const static byte DriveForward = 0x02;
  const static byte DriveBackward = 0x03;
  const static byte DriveLeft = 0x04;
  const static byte DriveRight = 0x05;
  const static byte FrontDiodeOn = 0x06;
  const static byte FrontDiodeOff = 0x07;
  const static byte StopMotors = 0x08;
} controlCodes;

void setup() {
  Serial.begin(9600);
  pinMode(FRONT_DIODE_PIN, OUTPUT);
  
  pinMode(LEFT_MOTOR_ENABLE_PIN, OUTPUT);
  pinMode(LEFT_MOTOR_INPUT_ONE_PIN, OUTPUT);
  pinMode(LEFT_MOTOR_INPUT_TWO_PIN, OUTPUT);
  
  pinMode(RIGHT_MOTOR_ENABLE_PIN, OUTPUT);
  pinMode(RIGHT_MOTOR_INPUT_ONE_PIN, OUTPUT);
  pinMode(RIGHT_MOTOR_INPUT_TWO_PIN, OUTPUT);
}

void loop() {
  if (Serial.available() > 0) {
    byte controlCode = Serial.read();
    
    if (controlCode == controlCodes.Heartbeat) {
      if (!firstHeartbeatInConnectionReceived) {
        lastHeartbeatTransmitTime = millis();
        firstHeartbeatInConnectionReceived = true;
      }
      
      receiveHeartbeat();
    }
     
    if (controlCode == controlCodes.FrontDiodeOn) {
      if (!frontDiodeActivated) {
        setFrontDiode(true);
      }
    }
    
    if (controlCode == controlCodes.FrontDiodeOff) {
      if (frontDiodeActivated) {
        setFrontDiode(false);
      }
    }
    
    if (controlCode == controlCodes.DriveForward) {
      startLeftMotor(false);
      startRightMotor(false);
    }
    
    if (controlCode == controlCodes.DriveBackward) {
      startLeftMotor(true);
      startRightMotor(true);
    }
    
    if (controlCode == controlCodes.DriveLeft) {
      startLeftMotor(true);
      startRightMotor(false);
    }
    
    if (controlCode == controlCodes.DriveRight) {
      startLeftMotor(false);
      startRightMotor(true);
    }
    
    if (controlCode == controlCodes.StopMotors) {
      if (leftMotorActivated) {
        stopLeftMotor();
      }
      if (rightMotorActivated) {
        stopRightMotor();
      }
    }
  }
  
  if (firstHeartbeatInConnectionReceived) {
    checkHeartbeatTime();
    
    if (connected) {
      transmitHeartbeat();
    }
    
    if (!connected) {
      if (leftMotorActivated) {
        stopLeftMotor();
      }
      if (rightMotorActivated) {
        stopRightMotor();
      }
      if (frontDiodeActivated) {
        setFrontDiode(false);
      }
    }
  }
}

void setFrontDiode(boolean isOn) {
  if (isOn) {  
    digitalWrite(FRONT_DIODE_PIN, HIGH);
    frontDiodeActivated = true;
  } else {
    digitalWrite(FRONT_DIODE_PIN, LOW);
    frontDiodeActivated = false;
  }
}

void startLeftMotor(boolean reverse) {
  analogWrite(LEFT_MOTOR_ENABLE_PIN, 255);
  digitalWrite(LEFT_MOTOR_INPUT_ONE_PIN, !reverse);
  digitalWrite(LEFT_MOTOR_INPUT_TWO_PIN, reverse);
  leftMotorActivated = true;
}

void stopLeftMotor() {
  analogWrite(LEFT_MOTOR_ENABLE_PIN, 0);
  digitalWrite(LEFT_MOTOR_INPUT_ONE_PIN, LOW);
  digitalWrite(LEFT_MOTOR_INPUT_TWO_PIN, LOW);
  leftMotorActivated = false;
}

void startRightMotor(boolean reverse) {
  analogWrite(RIGHT_MOTOR_ENABLE_PIN, 255);
  digitalWrite(RIGHT_MOTOR_INPUT_ONE_PIN, reverse);
  digitalWrite(RIGHT_MOTOR_INPUT_TWO_PIN, !reverse);
  rightMotorActivated = true;
}

void stopRightMotor() {
  analogWrite(RIGHT_MOTOR_ENABLE_PIN, 0);
  digitalWrite(RIGHT_MOTOR_INPUT_ONE_PIN, LOW);
  digitalWrite(RIGHT_MOTOR_INPUT_TWO_PIN, LOW);
  rightMotorActivated = false;
}

void transmitHeartbeat() {
  unsigned long timeSinceStart = millis();
  
  if(timeSinceStart - lastHeartbeatTransmitTime >= HEARTBEAT_INTERVAL) {
    lastHeartbeatTransmitTime = timeSinceStart;
    Serial.write(controlCodes.Heartbeat);
  }
  
  if (timeSinceStart < lastHeartbeatTransmitTime) {
    lastHeartbeatTransmitTime = 0;
  }
}

void receiveHeartbeat() {
  unsigned long timeSinceStart = millis();
  lastHeartbeatReceiveTime = timeSinceStart;
}

void checkHeartbeatTime() {
  unsigned long timeSinceStart = millis();
  
  if (timeSinceStart - lastHeartbeatReceiveTime >= CONNECTION_TIMEOUT) {
    connected = false;
    firstHeartbeatInConnectionReceived = false;
  } else {
    connected = true;
  }
}
